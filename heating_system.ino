#include <Servo.h>
#include <stdio.h>
#include "rgb_lcd.h"
#include <TH02_dev.h>
#include <math.h>

#include <limits.h>

#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"

#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 250
#define MAX_MESSAGE_LENGTH 80

// Phase 1 variables
char message[MAX_MESSAGE_LENGTH];

const int pinMoistrue = A1;
const int pinButton = 0;
const int pinRelay = 5;
const int pinServo = 6;

float value_num;
bool value_bool;
float gas_cost = 0;
float real_gas_cost = 0;
float eletric_cost = 0;
float target_temperature = 0;
int remote_button = 0;
int local_button = 0;
float water_temperature = 0;
float moisture = 0;
bool isEletric = false;
bool isGas = false;
bool isOff = true;
bool handle_remote_button = false;
char system_status = 'O';

rgb_lcd lcd;
Servo myservo;

unsigned long debounceDelay = 200;
unsigned long lastDebounceTime = 0; 


// Phase 2 variables
static char certDirectory[] = "certs";

IoT_Error_t rc = FAILURE;
int32_t i = 0;

char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);
char *pJsonStringToUpdate;
float temperature = 0.0;

jsonStruct_t temperatureHandler;
jsonStruct_t gasCostHandler;
jsonStruct_t eletricCostHandler;
jsonStruct_t remoteButtonHandler;
jsonStruct_t targetTemperatureHandler;
jsonStruct_t systemStatusHandler;

AWS_IoT_Client mqttClient;


void setup() {
    Serial.begin(9600);

    lcd.begin(16, 2);
    pinMode(pinRelay,OUTPUT);

    myservo.attach(pinServo);
    myservo.write(90);

    pinMode(pinButton,INPUT);

    lastDebounceTime = millis();

    // Setup Json to communicate with aws
    setupJson();
    // Setup connection to aws
    setupConnection();

}

void handleButton(){
    unsigned currentMillis = millis();
    if ((currentMillis - lastDebounceTime) > debounceDelay) {
        //printf("CARLOS Button Interruption");
        if (digitalRead(pinButton) == 1) {
            local_button = local_button ^ 1;
        }
        if (local_button==0 && remote_button==0) {
            isOff = true;
        }
        lastDebounceTime = currentMillis;
    }
    handle_remote_button = false;
}
void loop() {
    // Read temperature and moisture from sensores to current loop
    getTempSensorValue();
    getMoisture();

    // Check id there is new text to be parsed in serial
    while (readFromSerial()) {
        parseMessage();
    }

    if (digitalRead(pinButton) == 1 || handle_remote_button) {
        handleButton();
    }

    // Calculate real value of Gas based on moisture
    calculateGasEfficiency();

    // Turn on/off relay and use cheaper boiler.
    setBoilerState();

    system_status = (isOff == true ? 'O': isGas ? 'G' : 'E');
    IOT_DEBUG("system_status %c", system_status);

    // Update state in LCD
    updateLcd();

    // Update state in Servo
    controlServo();

    sendDataAws();
}

void calculateBestHeater() {
    IOT_INFO("real_gas_cost %f eletric_cost %f", real_gas_cost, eletric_cost);
    if (!isOff) {
        if (real_gas_cost > eletric_cost) {
            isEletric = true;
            isGas = false;
        } else {
            isEletric = false;
            isGas = true;          
        }
    } else {
        isGas = false;
        isEletric = false;
    }  
}

/* Servo behavior
 *          OFF (90o)
 *           |
 * (180o)E---|---G (0o)
 */
void controlServo() {
    if (isOff) {
        myservo.write(90);
        return;
    }
    if (isGas) {
        myservo.write(0);
    } else if (isEletric) {
        myservo.write(180);
    }
}

void setBoilerState() {
    if (local_button || remote_button) {
        if (water_temperature < target_temperature) {
            digitalWrite(pinRelay,HIGH);
            isOff = false;
        } else {
            digitalWrite(pinRelay,LOW);
            isOff = true;
        }
        calculateBestHeater();
    } else {
        digitalWrite(pinRelay,LOW);
    }
}

void calculateGasEfficiency() {
    real_gas_cost = fabs((float)(gas_cost * ( 1 - ((float)moisture / 1000.0) * 8.0)));
}

bool readFromSerial() {
    int index = 0;
    while (Serial.available()) {
        message[index] = Serial.read();        
        if ( (message[index] == '\n' && index > 0) ||
             (++index >= MAX_MESSAGE_LENGTH - 1) ) {
            message[index] = '\0';
            return true;
        }
    }
    return false;
}

void parseMessage() {
    Serial.println(message);
    char command = message[0];
    
    if (command == 'G') {
        sscanf(message,"%c %f", &command, &gas_cost);      
    } else if (command == 'E') {
        sscanf(message,"%c %f", &command, &eletric_cost);
    } else if (command == 'T') {
        sscanf(message,"%c %f", &command, &target_temperature);
    } else if (command == 'R') {
        sscanf(message,"%c %d", &command, &remote_button);
        handle_remote_button = true;
    } else if (command == 'L') {
        sscanf(message,"%c %d", &command, &local_button);
    } else if (command == 'W') {
        sscanf(message,"%c %f", &command, &water_temperature);
    } else if (command == 'M') {
        sscanf(message,"%c %f", &command, &moisture);
    }
}

void buildLCDText() {
    char line1[20], line2[20];
    sprintf(line1, "%c%c%c   %1.2f C", getOff(), getGas(), getEletric(), water_temperature);
    sprintf(line2, "      %1.2f C", target_temperature);
    lcd.setCursor(0 ,0);
    lcd.print(line1);
    lcd.setCursor(0, 1);
    lcd.print(line2);
}

char getEletric() {
    return (isEletric && (!isOff)? 'E' : ' ');
}

char getGas() {
    return (isGas && (!isOff) ? 'G' : ' ');
}

char getOff() {
    return (isOff ? 'O' : ' ');
}

void updateLcd() {
    buildLCDText();
}


void getTempSensorValue(){
    water_temperature = TH02.ReadTemperature();
}
void getMoisture() {
    moisture = analogRead(pinMoistrue);
}

//===========================================================
// Phase 2 functions
//===========================================================

/**
 * Setup json data post and callbacks for publishes
 */
void setupJson() {
  
  temperatureHandler.cb = NULL;
  temperatureHandler.pKey = "water_temperature";
  temperatureHandler.pData = &water_temperature;
  temperatureHandler.type = SHADOW_JSON_FLOAT;

  targetTemperatureHandler.cb = targetTemperature_Callback;
  targetTemperatureHandler.pKey = "target_temperature";
  targetTemperatureHandler.pData = &target_temperature;
  targetTemperatureHandler.type = SHADOW_JSON_FLOAT;

  gasCostHandler.cb = gasCost_Callback;
  gasCostHandler.pKey = "gas_cost";
  gasCostHandler.pData = &real_gas_cost;
  gasCostHandler.type = SHADOW_JSON_FLOAT;

  eletricCostHandler.cb = eletricCost_Callback;
  eletricCostHandler.pKey = "eletric_cost";
  eletricCostHandler.pData = &eletric_cost;
  eletricCostHandler.type = SHADOW_JSON_FLOAT;

  remoteButtonHandler.cb = remoteButton_Callback;
  remoteButtonHandler.pKey = "remote_button";
  remoteButtonHandler.pData = &remote_button;
  remoteButtonHandler.type = SHADOW_JSON_INT8;

  systemStatusHandler.cb = NULL;
  systemStatusHandler.pKey = "system_status";
  systemStatusHandler.pData = &system_status;
  systemStatusHandler.type = SHADOW_JSON_INT8;

}

/**
 * Setup connection details
 */
void setupConnection() {

    char rootCA[PATH_MAX + 1];
    char clientCRT[PATH_MAX + 1];
    char clientKey[PATH_MAX + 1];
    char CurrentWD[PATH_MAX + 1] = "sketch";
  
    IOT_INFO("\nAWS IoT SDK Version %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

    // Certificates are located at /sketch/certs
    snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_ROOT_CA_FILENAME);
    snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
    snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

    IOT_DEBUG("rootCA %s", rootCA);
    IOT_DEBUG("clientCRT %s", clientCRT);
    IOT_DEBUG("clientKey %s", clientKey);

    // initialize the mqtt client
    ShadowInitParameters_t sp = ShadowInitParametersDefault;
    sp.pHost = AWS_IOT_MQTT_HOST;
    sp.port = AWS_IOT_MQTT_PORT;
    sp.pClientCRT = clientCRT;
    sp.pClientKey = clientKey;
    sp.pRootCA = rootCA;
    sp.enableAutoReconnect = false;
    sp.disconnectHandler = NULL;

    IOT_INFO("Shadow Init");
    rc = aws_iot_shadow_init(&mqttClient, &sp);
    if(SUCCESS != rc) {
        IOT_ERROR("Shadow Connection Error");
        return;
    }

    ShadowConnectParameters_t scp = ShadowConnectParametersDefault;
    scp.pMyThingName = AWS_IOT_MY_THING_NAME;
    scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
    scp.mqttClientIdLen = (uint16_t) strlen(AWS_IOT_MQTT_CLIENT_ID);

    IOT_INFO("Shadow Connect");
    rc = aws_iot_shadow_connect(&mqttClient, &scp);
    if(SUCCESS != rc) {
        IOT_ERROR("Shadow Connection Error");
        return;
    }

    /*
     * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
     *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
     *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
     */
    rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
    if(SUCCESS != rc) {
        IOT_ERROR("Unable to set Auto Reconnect to true - %d", rc);
        return;
    }

    rc = aws_iot_shadow_register_delta(&mqttClient, &remoteButtonHandler);
    rc = aws_iot_shadow_register_delta(&mqttClient, &gasCostHandler);
    rc = aws_iot_shadow_register_delta(&mqttClient, &eletricCostHandler);
    rc = aws_iot_shadow_register_delta(&mqttClient, &targetTemperatureHandler);
    if(SUCCESS != rc) {
        IOT_ERROR("Shadow Register Delta Error");
    }
}

/**
 * Callback to update the gas_cost from AWS
 */
void gasCost_Callback (const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        gas_cost = *(float *)pContext->pData;
        IOT_INFO("Delta - Changing state of gas_cost to %f %f", *(float *) (pContext->pData), gas_cost);
    }
}

/**
 * Callback to update the eletric_cost from AWS
 */
void eletricCost_Callback (const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        eletric_cost = *(float *)pContext->pData;
        IOT_INFO("Delta - Changing state of eletric_cost to %f %f", *(float *) (pContext->pData), eletric_cost);
    }
}


/**
 * Callback to update the target_temperature from AWS
 */
void targetTemperature_Callback (const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        target_temperature = *(float *)pContext->pData;
        IOT_INFO("Delta - Changing state of target_temperature to %f %f", *(float *) (pContext->pData), target_temperature);
    }
}

/**
 * Callback to update the remote_button from AWS
 */
void remoteButton_Callback(const char *pJsonString, uint32_t JsonStringDataLen, jsonStruct_t *pContext) {
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if(pContext != NULL) {
        remote_button = *(int *)pContext->pData;
        handle_remote_button = true;
        IOT_INFO("Delta - Changing state of remote_button to %d", *(int *) (pContext->pData), remote_button);
    }
}

/**
 * Sends data to AWS
 */
int sendDataAws() {
    rc = aws_iot_shadow_yield(&mqttClient, 200);
    if(NETWORK_ATTEMPTING_RECONNECT == rc) {
        sleep(1);
        // If the client is attempting to reconnect we will skip the rest of the loop.
        return rc;        
    }
    IOT_INFO("\n=======================================================================================\n");

    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
    if(SUCCESS == rc) {
        rc = aws_iot_shadow_add_reported(JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 6, &temperatureHandler,
                       &gasCostHandler, &eletricCostHandler, &remoteButtonHandler, &targetTemperatureHandler,
                       &systemStatusHandler);

        // IOT_INFO("Add report status %d", rc);
        if(SUCCESS == rc) {
            rc = aws_iot_finalize_json_document(JsonDocumentBuffer, sizeOfJsonDocumentBuffer);
            // IOT_INFO("Add report status %d", rc);
            if(SUCCESS == rc) {
                IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
                rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                         ShadowUpdateStatusCallback, NULL, 4, true);
            }
        }
    }
    IOT_INFO("*****************************************************************************************\n");
    sleep(1);
    return rc;
}

/**
 * Callback result of shadow update.
 * Prints the result of update.
 */
void ShadowUpdateStatusCallback(const char *pThingName, ShadowActions_t action, Shadow_Ack_Status_t status,
                const char *pReceivedJsonDocument, void *pContextData) {
    IOT_UNUSED(pThingName);
    IOT_UNUSED(action);
    IOT_UNUSED(pReceivedJsonDocument);
    IOT_UNUSED(pContextData);

    if(SHADOW_ACK_TIMEOUT == status) {
        IOT_INFO("Update Timeout--");
    } else if(SHADOW_ACK_REJECTED == status) {
        IOT_INFO("Update RejectedXX");
    } else if(SHADOW_ACK_ACCEPTED == status) {
        IOT_INFO("Update Accepted !!");
    }
}

